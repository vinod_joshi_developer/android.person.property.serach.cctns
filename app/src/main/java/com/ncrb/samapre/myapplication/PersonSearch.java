package com.ncrb.samapre.myapplication;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedInput;

import android.app.ProgressDialog;

public class PersonSearch extends AppCompatActivity {
    Spinner sp_Gender, sp_Relation_Type,sp_Religion,Sp_PS,Sp_Dist;


    CheckBox chkArr;
    CheckBox chkCon;
    CheckBox chkPO;
    CheckBox chkCS;
    CheckBox chkHO;


    EditText edt_person_first_name,edt_person_middle_name,edt_person_last_name, edt_person_alias, edt_Relative_Name;
    //EditText edt_person_uid;
    EditText edt_person_age_From, edt_person_age_To;
    EditText edt_person_height_From_Ft, edt_person_height_From_Inch,edt_person_height_To_Ft,edt_person_height_To_Inch;
    StringBuilder out1,out2,outDist,outPS;
    String relation="relationtype";
    String religion="religiontype";
    ArrayList<String> RelTypeData = new ArrayList<>();
    ArrayList<String> RelTypeCode = new ArrayList<>();
    ArrayList<String> ReligionData = new ArrayList<>();
    ArrayList<String> ReligionCode = new ArrayList<>();

    ArrayList<String> DistrictData = new ArrayList<>();
    ArrayList<String> DistrictCode = new ArrayList<>();
    ArrayList<String> PSData = new ArrayList<>();
    ArrayList<String> PSCode = new ArrayList<>();

    int GCode, GenCode, RelgnCode,RelTypCode;
    String GenderSelected;

    Object RCode,RTypeCode,Dist_Code,PS_Code;
    Button button_person_search;
    ImageButton btn_Back;

    Integer Dis_Code,PSIn_Code=0;

    public ProgressDialog mProgressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_person_search);


        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Please wait...");


        // ask user to select lang
        //selectLangCd();


        chkArr=(CheckBox) findViewById(R.id.chkArrested);
        chkCon=(CheckBox) findViewById(R.id.chkConvicted);
        chkPO=(CheckBox) findViewById(R.id.chkPO);
        chkCS=(CheckBox) findViewById(R.id.chkCS);
        chkHO=(CheckBox) findViewById(R.id.chkHO);

        btn_Back = (ImageButton) findViewById(R.id.btn_Back);
        edt_person_first_name=(EditText)findViewById(R.id.edt_FName);
        edt_person_middle_name=(EditText)findViewById(R.id.edt_MName);
        edt_person_last_name=(EditText)findViewById(R.id.edt_LName);
        edt_person_alias=(EditText)findViewById(R.id.edt_Alias);
        //edt_person_uid=(EditText)findViewById(R.id.edt_Uid);
        edt_person_age_From=(EditText)findViewById(R.id.edt_age_From);
        edt_person_age_To=(EditText)findViewById(R.id.edt_age_To);
        edt_person_height_From_Ft=(EditText)findViewById(R.id.edt_height_From_Ft);
        edt_person_height_From_Inch=(EditText)findViewById(R.id.edt_height_From_Inch);
        edt_person_height_To_Ft=(EditText)findViewById(R.id.edt_height_To_Ft);
        edt_person_height_To_Inch=(EditText)findViewById(R.id.edt_height_To_Inch);

        button_person_search=(Button)findViewById(R.id.btn_Person_Search);

        sp_Relation_Type=(Spinner) findViewById(R.id.sp_Relation_Type);
        sp_Religion=(Spinner) findViewById(R.id.sp_Religion);
        edt_Relative_Name=(EditText)findViewById(R.id.edt_Rel_Name);

        Sp_PS = (Spinner) findViewById(R.id.PS);
        Sp_Dist = (Spinner) findViewById(R.id.District);



        sp_Gender=(Spinner) findViewById(R.id.sp_Gender);
        ArrayList<String> genderList = new ArrayList<String>();
        genderList.add("--Select--");
        genderList.add("Male");
        genderList.add("Female");
        genderList.add("Transgender");
        genderList.add("Unknown");

        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, genderList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        sp_Gender.setAdapter(adapter);

        try {
            GetRelationTypeWebService(relation);
            //GetReligionTypeWebService(religion);
        } catch (Exception e) {
            e.printStackTrace();
        }


        Sp_Dist.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {
                //  Object item = adapterView.getItemAtPosition(position);

                Dist_Code=DistrictCode.get(position);

                Integer DCode;
                DCode=Integer.parseInt(Dist_Code.toString());

                if (DCode!=0) {
                    try {
                        PSCode.clear();
                        PSData.clear();
                        Sp_PS.setAdapter(null);

                        GetPSWebService(DCode);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // TODO Auto-generated method stub

            }
        });

        Sp_PS.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {

                PS_Code=PSCode.get(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // TODO Auto-generated method stub

            }
        });

        sp_Gender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {
                //  Object item = adapterView.getItemAtPosition(position);

                GenderSelected=((Spinner)findViewById(R.id.sp_Gender)).getSelectedItem().toString();

                if(GenderSelected=="Male")
                {
                    GCode=3;
                }
                else if(GenderSelected=="Female")
                {
                    GCode=2;
                }
                else if(GenderSelected=="Transgender")
                {
                    GCode=1;
                }
                else if(GenderSelected=="Unknown")
                {
                    GCode=4;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // TODO Auto-generated method stub

            }
        });

        sp_Religion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {
                //  Object item = adapterView.getItemAtPosition(position);

                RCode=ReligionCode.get(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // TODO Auto-generated method stub

            }
        });

        sp_Relation_Type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {
                //  Object item = adapterView.getItemAtPosition(position);

                RTypeCode=RelTypeCode.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // TODO Auto-generated method stub

            }
        });


        btn_Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }});


        button_person_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                {

                    Intent intent = new Intent(getApplicationContext(), PersonDisplay.class);
                    Bundle bundle = new Bundle();

                    bundle.putString("P_FName", edt_person_first_name.getText().toString());
                    bundle.putString("P_MName", edt_person_middle_name.getText().toString());
                    bundle.putString("P_LName", edt_person_last_name.getText().toString());
                    bundle.putString("P_Alias", edt_person_alias.getText().toString());
                    if (GenderSelected!=null)
                        GenCode = GCode;
                    else
                        GenCode = 0;
                    bundle.putInt("P_GCODE", (int)GenCode);
                    if (RCode!=null)
                        RelgnCode = Integer.parseInt(RCode.toString());
                    else
                        RelgnCode = 0;
                    bundle.putInt("P_RELGNCODE", RelgnCode);
                    if (RTypeCode!=null)
                        RelTypCode = Integer.parseInt(RTypeCode.toString());
                    else
                        RelTypCode = 0;
                    bundle.putInt("P_RELTYPECODE", RelTypCode);
                    bundle.putString("P_RelativeName", edt_Relative_Name.getText().toString());
                    if (edt_person_age_From.getText().length()==0)

                        edt_person_age_From.setText("0");
                    bundle.putInt("P_AgeFrom", Integer.parseInt(edt_person_age_From.getText().toString()));
                    if (edt_person_age_To.getText().length()==0)
                        edt_person_age_To.setText("0");
                    bundle.putInt("P_AgeTo",  Integer.parseInt(edt_person_age_To.getText().toString()));

                    if (edt_person_height_From_Ft.getText().length()==0)
                        edt_person_height_From_Ft.setText("0");
                    if (edt_person_height_From_Inch.getText().length()==0)
                        edt_person_height_From_Inch.setText("0");
                    int Height_From_Ft=Integer.parseInt(edt_person_height_From_Ft.getText().toString());
                    int Height_From_Inch=Integer.parseInt(edt_person_height_From_Inch.getText().toString());
                    double Height_From_cm=2.54*((Height_From_Ft*12)+Height_From_Inch);
                    bundle.putDouble("P_HEIGHTFROM", Height_From_cm);
                    if (edt_person_height_To_Ft.getText().length()==0)
                        edt_person_height_To_Ft.setText("0");
                    if (edt_person_height_To_Inch.getText().length()==0)
                        edt_person_height_To_Inch.setText("0");
                    int Height_To_Ft=Integer.parseInt(edt_person_height_To_Ft.getText().toString());
                    int Height_To_Inch=Integer.parseInt(edt_person_height_To_Inch.getText().toString());
                    double Height_To_cm=2.54*((Height_To_Ft*12)+Height_To_Inch);
                    bundle.putDouble("P_HEIGHTTO", Height_To_cm);


                    if (Dist_Code!=null)
                        Dis_Code = Integer.parseInt(Dist_Code.toString());
                    else
                        Dis_Code = 0;
                    if (PS_Code!=null)
                        PSIn_Code = Integer.parseInt(PS_Code.toString());
                    else
                        PSIn_Code = 0;

                    bundle.putInt("Dist_Code", Dis_Code);
                    bundle.putInt("PS_Code", PSIn_Code);

                    bundle.putBoolean("Arr", chkArr.isChecked());
                    bundle.putBoolean("Con", chkCon.isChecked());
                    bundle.putBoolean("PO", chkPO.isChecked());
                    bundle.putBoolean("CS", chkCS.isChecked());
                    bundle.putBoolean("HO", chkHO.isChecked());

                    //ask user what lang in which search happen
                    bundle.putString("lang_cd", ""+Constants.LANG_CD);

                    intent.putExtras(bundle);


                    startActivity(intent);
                }

//                if (chkArr.isChecked() || chkCon.isChecked() || chkPO.isChecked()||chkCS.isChecked()||chkHO.isChecked()) {
//
//                }
//                else
//                {
//                    Toast.makeText(getApplicationContext(),"Please select any of the search criteria", Toast.LENGTH_LONG).show();
//                }
            }
        });


    }// on create


    public void selectLangCd(){

        final CharSequence options[] = new CharSequence[] {"English", "Hindi"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setTitle("Select Language:");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // the user clicked on options[which]
                Utils.printv(""+options[which]);

                if( which == 1 ) Constants.LANG_CD = Constants.LANG_CD_HINDI;

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //the user clicked on Cancel
            }
        });
        builder.show();


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        return;
    }

    public void GetRelationTypeWebService(String relation) throws Exception {
       System.out.println("relation");

        this.mProgressDialog.show();

        Map postParams1 = new HashMap();
        postParams1.put("relation", relation);
        postParams1.put("m_service", "mRelationTypeConnect");

        JSONPostParams jsonPostParams1 = new JSONPostParams("mRelationTypeConnect", postParams1);

        // -----------------------------------------------------------------

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Constants.API_BASE_URL).setLog(new RestAdapter.Log() {
                    @Override
                    public void log(String msg) {
                        Log.i("Res Complaint -", msg);
                    }
                }).setLogLevel(RestAdapter.LogLevel.FULL).build();

        ApiCaller apiCaller = restAdapter.create(ApiCaller.class);


        apiCaller.mRelationTypeConnect(jsonPostParams1, new Callback<WSPLoginConnect>() {

            @Override
            public void failure(RetrofitError arg0) {

                if (mProgressDialog != null && mProgressDialog.isShowing())
                    mProgressDialog.dismiss();

                Toast.makeText(getApplicationContext(), "Can not connect to server for relation type.", Toast.LENGTH_LONG).show();


            }// end failure

            @Override
            public void success(WSPLoginConnect result, Response response) {

                System.out.println("RESULT status " + result.STATUS_CODE);

                if (result.STATUS_CODE.toString().equals("200")) {

                    if (mProgressDialog != null && mProgressDialog.isShowing())
                        mProgressDialog.dismiss();

                    TypedInput body = response.getBody();
                    try {
                        BufferedReader reader = new BufferedReader(new InputStreamReader(body.in()));
                        out1 = new StringBuilder();
                        String newLine = System.getProperty("line.separator");
                        String line;

                        while ((line = reader.readLine()) != null) {
                            out1.append(line);
                            out1.append(newLine);
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    String code;

                    try {
                        JSONObject reader = new JSONObject(out1.toString());
                        JSONArray states = reader.getJSONArray("RelationTypeList");

                        // set default value
                        RelTypeData.add("--Select--");
                        RelTypeCode.add("0");

                        for (int i = 0; i < states.length(); i++) {
                            JSONObject jsonObj2 = states.getJSONObject(i);
                            RelTypeData.add(new String(jsonObj2.getString("RelationType")));
                            RelTypeCode.add(new String(jsonObj2.getString("RelationTypeCd")));

                        }
                        System.out.println("CODE is is " + RelTypeCode );
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_spinner_item,
                            RelTypeData); //selected item will look like a spinner set from XML
                    spinnerArrayAdapter.setDropDownViewResource(android.R.layout
                            .simple_spinner_dropdown_item);
                    sp_Relation_Type.setAdapter(spinnerArrayAdapter);

                    try {
                        PersonSearch.this.GetReligionTypeWebService(religion);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                else {

                    if (mProgressDialog != null && mProgressDialog.isShowing())
                        mProgressDialog.dismiss();

                    Toast.makeText(getApplicationContext(), "Try Again", Toast.LENGTH_LONG).show();


                }

                //fetch district after success of previous call

                try {
                    PersonSearch.this.GetDistrictWebService(Constants.STATE_CD);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }// end success
        });

    }
    public void GetReligionTypeWebService(String religion) throws Exception {

        this.mProgressDialog.show();

        Map postParams1 = new HashMap();
        postParams1.put("religion", religion);
        postParams1.put("m_service", "mReligionConnect");

        JSONPostParams jsonPostParams1 = new JSONPostParams("mReligionConnect", postParams1);

        // -----------------------------------------------------------------

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Constants.API_BASE_URL).setLog(new RestAdapter.Log() {
                    @Override
                    public void log(String msg) {
                        Log.i("Res Complaint -", msg);
                    }
                }).setLogLevel(RestAdapter.LogLevel.FULL).build();

        ApiCaller apiCaller = restAdapter.create(ApiCaller.class);


        apiCaller.mReligionConnect(jsonPostParams1, new Callback<WSPReligionConnect>() {

            @Override
            public void failure(RetrofitError arg0) {

                if (mProgressDialog != null && mProgressDialog.isShowing())
                    mProgressDialog.dismiss();

                Toast.makeText(getApplicationContext(), "Can not connect to server for religion type.", Toast.LENGTH_LONG).show();



            }// end failure

            @Override
            public void success(WSPReligionConnect result, Response response) {

                System.out.println("RESULT status " + result.STATUS_CODE);

                if (result.STATUS_CODE.toString().equals("200")) {


                    if (mProgressDialog != null && mProgressDialog.isShowing())
                        mProgressDialog.dismiss();

                    TypedInput body = response.getBody();
                    try {
                        BufferedReader reader = new BufferedReader(new InputStreamReader(body.in()));
                        out2 = new StringBuilder();
                        String newLine = System.getProperty("line.separator");
                        String line;

                        while ((line = reader.readLine()) != null) {
                            out2.append(line);
                            out2.append(newLine);
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    String code;

                    try {
                        JSONObject reader = new JSONObject(out2.toString());
                        JSONArray states = reader.getJSONArray("ReligionList");

                        // set default value
                        ReligionData.add("--Select--");
                        ReligionCode.add("0");

                        for (int i = 0; i < states.length(); i++) {
                            JSONObject jsonObj2 = states.getJSONObject(i);
                            ReligionData.add(new String(jsonObj2.getString("ReligionName")));
                            ReligionCode.add(new String(jsonObj2.getString("ReligionCd")));

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_spinner_item,
                            ReligionData); //selected item will look like a spinner set from XML
                    spinnerArrayAdapter.setDropDownViewResource(android.R.layout
                            .simple_spinner_dropdown_item);
                    sp_Religion.setAdapter(spinnerArrayAdapter);


                } else {

                    if (mProgressDialog != null && mProgressDialog.isShowing())
                        mProgressDialog.dismiss();

                    Toast.makeText(getApplicationContext(), "Try Again", Toast.LENGTH_LONG).show();

                }
            }// end success
        });

    }

    public void GetDistrictWebService(String State) throws Exception {

        this.mProgressDialog.show();
        Map postParams1 = new HashMap();
        postParams1.put("FxdState_Cd", State);
        postParams1.put("m_service", "mDistrictConnect");

        JSONPostParams jsonPostParams1 = new JSONPostParams("mDistrictConnect", postParams1);

        // -----------------------------------------------------------------

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Constants.API_BASE_URL).setLog(new RestAdapter.Log() {
                    @Override
                    public void log(String msg) {
                        Log.i("Res Complaint -", msg);
                    }
                }).setLogLevel(RestAdapter.LogLevel.FULL).build();

        ApiCaller apiCaller = restAdapter.create(ApiCaller.class);


        apiCaller.mDistrictConnect(jsonPostParams1, new Callback<WSPLoginConnect>() {

            @Override
            public void failure(RetrofitError arg0) {
                if (mProgressDialog != null && mProgressDialog.isShowing())
                    mProgressDialog.dismiss();
                Toast.makeText(getApplicationContext(), "Can not connect to server.", Toast.LENGTH_LONG).show();


            }// end failure

            @Override
            public void success(WSPLoginConnect result, Response response) {
                if (result.STATUS_CODE.toString().equals("200")) {

                    if (mProgressDialog != null && mProgressDialog.isShowing())
                        mProgressDialog.dismiss();
                    TypedInput body = response.getBody();
                    try {
                        BufferedReader reader = new BufferedReader(new InputStreamReader(body.in()));
                        outDist = new StringBuilder();
                        String newLine = System.getProperty("line.separator");
                        String line;

                        while ((line = reader.readLine()) != null) {
                            outDist.append(line);
                            outDist.append(newLine);
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    String code;

                    try {
                        JSONObject reader = new JSONObject(outDist.toString());
                        JSONArray states = reader.getJSONArray("DistrictList");

                        // set default value
                        DistrictData.add("--Select--");
                        DistrictCode.add("0");

                        for (int i = 0; i < states.length(); i++) {
                            JSONObject jsonObj2 = states.getJSONObject(i);
                            DistrictData.add(new String(jsonObj2.getString("DistrictName")));
                            DistrictCode.add(new String(jsonObj2.getString("DistrictCd")));

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }




                    ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_spinner_item,
                            DistrictData); //selected item will look like a spinner set from XML
                    spinnerArrayAdapter.setDropDownViewResource(android.R.layout
                            .simple_spinner_dropdown_item);
                    Sp_Dist.setAdapter(spinnerArrayAdapter);


                    System.out.println("RESULT status " + result.STATUS_CODE);






                } else {
                    if (mProgressDialog != null && mProgressDialog.isShowing())
                        mProgressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), "Try Again", Toast.LENGTH_LONG).show();


                }
            }// end success
        });

    }

    public void GetPSWebService(Integer DCode) throws Exception {
        this.mProgressDialog.show();

        Map postParams1 = new HashMap();
        postParams1.put("District_Cd", DCode);
        postParams1.put("m_service", "mPoliceStationConnect");

        JSONPostParams jsonPostParams1 = new JSONPostParams("mPoliceStationConnect", postParams1);

        // -----------------------------------------------------------------

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Constants.API_BASE_URL).setLog(new RestAdapter.Log() {
                    @Override
                    public void log(String msg) {
                        Log.i("Res Complaint -", msg);
                    }
                }).setLogLevel(RestAdapter.LogLevel.FULL).build();

        ApiCaller apiCaller = restAdapter.create(ApiCaller.class);


        apiCaller.mPoliceStationConnect(jsonPostParams1, new Callback<WSPLoginConnect>() {

            @Override
            public void failure(RetrofitError arg0) {
                if (mProgressDialog != null && mProgressDialog.isShowing())
                    mProgressDialog.dismiss();
                Toast.makeText(getApplicationContext(), "Can not connect to server.", Toast.LENGTH_LONG).show();


            }// end failure

            @Override
            public void success(WSPLoginConnect result, Response response) {
                if (result.STATUS_CODE.toString().equals("200")) {
                    if (mProgressDialog != null && mProgressDialog.isShowing())
                        mProgressDialog.dismiss();
                    TypedInput body = response.getBody();
                    try {
                        BufferedReader reader = new BufferedReader(new InputStreamReader(body.in()));
                        outPS = new StringBuilder();
                        String newLine = System.getProperty("line.separator");
                        String line;

                        while ((line = reader.readLine()) != null) {
                            outPS.append(line);
                            outPS.append(newLine);
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    String code;

                    try {
                        JSONObject reader = new JSONObject(outPS.toString());
                        JSONArray states = reader.getJSONArray("PoliceStationList");

                        // set default value
                        PSData.add("--Select--");
                        PSCode.add("0");

                        for (int i = 0; i < states.length(); i++) {
                            JSONObject jsonObj2 = states.getJSONObject(i);
                            PSData.add(new String(jsonObj2.getString("PoliceStationName")));
                            PSCode.add(new String(jsonObj2.getString("PoliceStationCd")));

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }




                    ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_spinner_item,
                            PSData); //selected item will look like a spinner set from XML
                    spinnerArrayAdapter.setDropDownViewResource(android.R.layout
                            .simple_spinner_dropdown_item);
                    Sp_PS.setAdapter(spinnerArrayAdapter);


                    System.out.println("RESULT status " + result.STATUS_CODE);







                } else {
                    if (mProgressDialog != null && mProgressDialog.isShowing())
                        mProgressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), "Try Again", Toast.LENGTH_LONG).show();


                }
            }// end success
        });

    }

}
