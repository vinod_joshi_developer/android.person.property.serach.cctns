package com.ncrb.samapre.myapplication;

/**
 * Created by Lenovo on 16-02-2018.
 */

public class Constants  {

    // for MS stack
    //public static String API_BASE_URL = "http://10.23.74.68/ppSearch/WebService.asmx";

    // for java stack
    //public static String API_BASE_URL = "http://192.168.2.2:8080/cctns";
    public static String API_BASE_URL = "http://10.23.72.58:8080/cctns";

    public static String STACK_JAVA = "STACK_JAVA";
    public static String STACK_MS = "STACK_MS";

    // for JAVA OR MS STACK ASP.NET
    public static String API_PROGRAMMING_STACK = STACK_JAVA;

    public static String SearchTypePerson = "Person";

    public static String SearchTypeProperty = "Property";

    public static String STATE_CD = "27";// rajasthan

    public static String LANG_CD = "99";// HINDI =6 ENGLISH=99

    public static String LANG_CD_HINDI = "6";// HINDI =6 ENGLISH=99

    public static String LANG_CD_ENG = "99";// HINDI =6 ENGLISH=99




}// end
