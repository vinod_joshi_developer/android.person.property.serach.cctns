package com.ncrb.samapre.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.spec.PSSParameterSpec;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedInput;
import android.app.ProgressDialog;

import com.google.gson.Gson;

public class PropertyDisplay extends AppCompatActivity {

    ImageButton btn_Back;
    Integer VTCODE,DistCode,PSCode;
    Integer Manufacturer;
    Integer Model,Color_Code;
    String RegistrationNumber;
    String ChasisNo;
    String EngineNo;
    String ChkUAP;
    String ChkPS;
    String ChkFIR;
    StringBuilder out;
    Integer STATE_CD=11;

    TextView title;

    public ProgressDialog mProgressDialog;
    public int TOTAL_LIST_ITEMS;
    public int NUM_ITEMS_PAGE   = 15;
    private int noOfBtns;
    private Button[] btns;

    private PropertySearchInfo propertySearchInfo;
    private ArrayList<PropertySearchInfo> arrayList_property_search=new ArrayList<>();
    private ArrayList<PropertySearchInfo> sort=new ArrayList<PropertySearchInfo>();
    private PropertySearchAdapter   propertySearchAdapter,PropertyPageAdapter;
    private ListView listview_property_search;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_property_display);
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Please wait...");
        listview_property_search=(ListView)findViewById(R.id.list_search_result) ;
        btn_Back = (ImageButton) findViewById(R.id.btn_Back);
        title = (TextView)findViewById(R.id.title);



        Bundle bundle = getIntent().getExtras();
        VTCODE=bundle.getInt("VTCODE");
        Color_Code=bundle.getInt("C_Code");
        Manufacturer=bundle.getInt("Manufacturer");
        Model=bundle.getInt("Model");
        RegistrationNumber=bundle.getString("RegistrationNumber");
        DistCode=bundle.getInt("Di_Code");
        PSCode=bundle.getInt("P_Code");

        if (RegistrationNumber.isEmpty())
        {
            RegistrationNumber="0";
        }

        ChasisNo=bundle.getString("ChasisNo");
        if (ChasisNo.isEmpty())
        {
            ChasisNo="0";
        }
        EngineNo=bundle.getString("EngineNo");
        if (EngineNo.isEmpty())
        {
            EngineNo="0";
        }
        Boolean UAP=bundle.getBoolean("UAP");
        Boolean PS=bundle.getBoolean("PS");
        Boolean REGISTRATION=bundle.getBoolean("REGISTRATION");
        if (UAP) {
            ChkUAP="true";
        }
        else
        {
            ChkUAP="false";
        }
        if (PS) {
            ChkPS="true";
        }
        else
        {
            ChkPS ="false";
        }
        if (REGISTRATION) {
            ChkFIR="true";
        }
        else
        {
            ChkFIR ="false";
        }


        try {
            GetAuthDetailWebService();

        } catch (Exception e) {
            e.printStackTrace();
        }

        //Btnfooter();
        btn_Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        listview_property_search.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent=new Intent(PropertyDisplay.this,PropertyDetails.class);


                intent.putExtra("FIR_NO",propertySearchAdapter.getItem(position).getFIR_NO());
                intent.putExtra("FIR_REGN_NUM",propertySearchAdapter.getItem(position).getFIR_REGN_NUM());
                intent.putExtra("Vehicle_Type",propertySearchAdapter.getItem(position).getMV_Type());
                intent.putExtra("OWNER_NAME",propertySearchAdapter.getItem(position).getFirstName());
                intent.putExtra("Registration_Number",propertySearchAdapter.getItem(position).getRegistrationNo());
                intent.putExtra("CHASSIS_NO",propertySearchAdapter.getItem(position).getChasisNo());
                intent.putExtra("ENGINE_NO",propertySearchAdapter.getItem(position).getEngineNo());
                intent.putExtra("Full_FIR_NUMBER",propertySearchAdapter.getItem(position).getFull_FIR_NUMBER());
                intent.putExtra("State",propertySearchAdapter.getItem(position).getSTATE());
                intent.putExtra("district",propertySearchAdapter.getItem(position).getDistrict());
                intent.putExtra("PS",propertySearchAdapter.getItem(position).getPs());
                intent.putExtra("propertyNature",propertySearchAdapter.getItem(position).getPropertyNature());
                intent.putExtra("mvModel",propertySearchAdapter.getItem(position).getMvModel());
                intent.putExtra("mvMake",propertySearchAdapter.getItem(position).getMvMake());
                intent.putExtra("mvColor",propertySearchAdapter.getItem(position).getMvColor());
                intent.putExtra("GD_No",propertySearchAdapter.getItem(position).getGD_No());
                intent.putExtra("mvType",propertySearchAdapter.getItem(position).getMvType());

                startActivity(intent);
            }
        });
    }
    public void GetAuthDetailWebService() throws Exception {
        this.mProgressDialog.show();
        Map postParams = new HashMap();

        postParams.put("MATCH_CRITERIA_UAP",ChkUAP.toString());
        postParams.put("MATCH_CRITERIA_SEIZED",ChkPS.toString());
        postParams.put("MATCH_CRITERIA_REGISTERED",ChkFIR.toString());
        postParams.put("MV_TYPECD",VTCODE);
        postParams.put("MV_MAKECD",Manufacturer);
        postParams.put("MV_MODELCD",Model);
        postParams.put("REGISTRATION_NO",RegistrationNumber.toString());
        postParams.put("CHASSIS_NO",ChasisNo.toString());
        postParams.put("ENGINE_NO",EngineNo.toString());
        //postParams.put("ENGINE_NO",EngineNo.toString());
        postParams.put("MVCOLOR_CD",Color_Code);
        postParams.put("STATE_CD",STATE_CD);
        postParams.put("DISTRICT_CD",DistCode);
        postParams.put("PS_CD",PSCode);
        postParams.put("m_service", "mPropertySearch");


        JSONPostParams jsonPostParams = new JSONPostParams("mPropertySearch", postParams);


        // -----------------------------------------------------------------

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Constants.API_BASE_URL).setLog(new RestAdapter.Log() {
                    @Override
                    public void log(String msg) {
                        Log.i("Res Complaint -", msg);
                    }
                }).setLogLevel(RestAdapter.LogLevel.FULL).build();

        ApiCaller apiCaller = restAdapter.create(ApiCaller.class);

        int cnt;
        apiCaller.mPropertySearch(jsonPostParams, new Callback<WSPLoginConnect>() {

                    @Override
                    public void failure(RetrofitError arg0) {
                        if (mProgressDialog != null && mProgressDialog.isShowing())
                            mProgressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Can not connect to server.", Toast.LENGTH_LONG).show();


                    }// end failure

                    @Override
                    public void success(WSPLoginConnect result, Response response) {
                        if (result.STATUS_CODE.toString().equals("200")) {
                            if (mProgressDialog != null && mProgressDialog.isShowing())
                                mProgressDialog.dismiss();
                            TypedInput body = response.getBody();
                            try {
                                BufferedReader reader = new BufferedReader(new InputStreamReader(body.in()));
                                out = new StringBuilder();
                                String newLine = System.getProperty("line.separator");
                                String line;

                                while ((line = reader.readLine()) != null) {
                                    out.append(line);
                                    out.append(newLine);
                                }
                                System.out.println("out is is " + out.toString());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            String code;

                            try {

                                JSONObject reader = new JSONObject(out.toString());

                                JSONArray states = reader.getJSONArray("propertySearchCheck");

                                for (int i = 0; i < states.length(); i++) {

                                    JSONObject jsonObj2 = states.getJSONObject(i);

                                    Utils.printv("jsonObj2 json "+jsonObj2);

                                    Gson gson = new Gson();
                                    PropertySearchInfo objPropertySearchInfo = gson.fromJson(jsonObj2.toString(), PropertySearchInfo.class);

                                    arrayList_property_search.add(objPropertySearchInfo);

                                    // no need to write below way -->


                                    /*
                                    arrayList_property_search.add(
                                            new PropertySearchInfo(
                                            new String(jsonObj2.getString("FIR_REGN_NUM")),
                                            new String(jsonObj2.getString("GD_No")),
                                            new String(jsonObj2.getString("REG_OWNER")),
                                            new String(jsonObj2.getString("MV_TYPE")),
                                            new String(jsonObj2.getString("REGISTRATION_NO")),
                                            new String(jsonObj2.getString("CHASSIS_NO")),
                                            new String(jsonObj2.getString("ENGINE_NO")),
                                            new String(jsonObj2.getString("Full_FIR_NUMBER")),
                                            new String(jsonObj2.getString("State")),
                                            new String(jsonObj2.getString("district")),
                                            new String(jsonObj2.getString("ps")),
                                            new String(jsonObj2.getString("propertyNature")),
                                            new String(jsonObj2.getString("mvModel")),
                                            new String(jsonObj2.getString("mvMake")),
                                            new String(jsonObj2.getString("mvColor"))
                                    ));*/

                                } //end forloop

                                Btnfooter((int)states.length());
                                loadList(0);
                                CheckBtnBackGroud(0);
                                // propertySearchAdapter= new PropertySearchAdapter(getApplicationContext(),sort);
                                //listview_property_search.setAdapter(propertySearchAdapter);


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            System.out.println("RESULT status " + result.STATUS_CODE);







                        } else {
                            if (mProgressDialog != null && mProgressDialog.isShowing())
                                mProgressDialog.dismiss();
                            Toast.makeText(getApplicationContext(), "Try Again", Toast.LENGTH_LONG).show();


                        }
                    }// end success

                }

        );

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        return;
    }


    private void Btnfooter(int count)
    {

        TOTAL_LIST_ITEMS=count;
        int val = TOTAL_LIST_ITEMS%NUM_ITEMS_PAGE;
        val = val==0?0:1;
        noOfBtns=TOTAL_LIST_ITEMS/NUM_ITEMS_PAGE+val;

        LinearLayout ll = (LinearLayout)findViewById(R.id.btnLay);

        btns = new Button[noOfBtns];

        for(int i=0;i<noOfBtns;i++)
        {
            btns[i] =   new Button(this);
            btns[i].setBackgroundColor(getResources().getColor(android.R.color.transparent));
            btns[i].setText(""+(i+1));

            LinearLayout.LayoutParams lp = new     LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            ll.addView(btns[i], lp);

            final int j = i;
            btns[j].setOnClickListener(new OnClickListener() {

                public void onClick(View v)
                {
                    loadList(j);
                    CheckBtnBackGroud(j);
                }
            });
        }

    }


    private void loadList(int number)
    {
        sort.clear();
        //ArrayList<String> sort = new PropertySearchInfo(String, String, String reg_Owner, String MV_Type, String REGISTRATION_NO, String CHASSIS_NO, String ENGINE_NO, String full_FIR_NUMBER, String state, String district, String ps, String propertyNature, String mvModel, String mvMake, String mvColor);
        int start = number * NUM_ITEMS_PAGE;
        for(int i=start;i<(start)+NUM_ITEMS_PAGE;i++)
        {
            if(i<arrayList_property_search.size())
            {
                sort.add(arrayList_property_search.get(i));
            }
            else
            {
                break;
            }
        }
        propertySearchAdapter= new PropertySearchAdapter(getApplicationContext(),sort);
        listview_property_search.setAdapter(propertySearchAdapter);
    }


    private void CheckBtnBackGroud(int index)
    {
        title.setText("Page "+(index+1)+" of "+noOfBtns);
        for(int i=0;i<noOfBtns;i++)
        {
            if(i==index)
            {
                // btns[index].setBackgroundDrawable(getResources().getDrawable(R.drawable.box_green));
                //btns[index].setTextColor(getResources().getColor(android.R.color.holo_orange_light));
                btns[i].setTextColor(getResources().getColor(android.R.color.white));
            }
            else
            {
                btns[i].setBackgroundColor(getResources().getColor(android.R.color.transparent));
                btns[i].setTextColor(getResources().getColor(android.R.color.black));
            }
        }

    }
}