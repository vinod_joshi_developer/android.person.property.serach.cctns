package com.ncrb.samapre.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedInput;
import android.app.ProgressDialog;

import com.google.gson.Gson;


public class PersonDisplay extends AppCompatActivity {
    String P_FName,P_MName,P_LName,P_Alias,P_RelativeName;
    Integer P_GCODE,P_RELGNCODE,P_RELTYPECODE,P_AgeFrom,P_AgeTo,DistCode,PSCode;
    Double P_HEIGHTFROM,P_HEIGHTTO;
    String ChkArr,ChkCon,ChkPO,ChkCS,ChkHO;
    StringBuilder out;
    Integer STATE_CD=0;

    String Count;
    private ListView listview_person_search;
    TextView title;
    ImageButton btn_Back;

    public int TOTAL_LIST_ITEMS;
    public int NUM_ITEMS_PAGE   = 15;
    private int noOfBtns;
    private Button[] btns;

    private ArrayList<PersonSearchInfo> arrayList_person_search=new ArrayList<PersonSearchInfo>();
    private ArrayList<PersonSearchInfo> sort=new ArrayList<PersonSearchInfo>();
    private PersonSearchAdapter   personSearchAdapter;
    public ProgressDialog mProgressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person_display);
        listview_person_search=(ListView)findViewById(R.id.list_person_search_result) ;
        btn_Back = (ImageButton) findViewById(R.id.btn_Back);
        title = (TextView)findViewById(R.id.title);

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Please wait...");
                Bundle bundle = getIntent().getExtras();

                P_FName=bundle.getString("P_FName");
                if (P_FName.isEmpty())
                {
                    P_FName="0";
                }
                P_MName=bundle.getString("P_MName");
                if (P_MName.isEmpty())
                {
                    P_MName="0";
                }
                P_LName=bundle.getString("P_LName");
                if (P_LName.isEmpty())
                {
                    P_LName="0";
                }
                P_Alias=bundle.getString("P_Alias");
                if (P_Alias.isEmpty())
                {
                    P_Alias="0";
                }
                P_GCODE=bundle.getInt("P_GCODE");

                P_RELGNCODE=bundle.getInt("P_RELGNCODE");

                P_RELTYPECODE=bundle.getInt("P_RELTYPECODE");

                P_RelativeName=bundle.getString("P_RelativeName");

                if (P_RelativeName.isEmpty())
                {
                    P_RelativeName="0";
                }
                P_AgeFrom=bundle.getInt("P_AgeFrom");
                P_AgeTo=bundle.getInt("P_AgeTo");
                P_HEIGHTFROM=bundle.getDouble("P_HEIGHTFROM");
                P_HEIGHTTO=bundle.getDouble("P_HEIGHTTO");
        DistCode=bundle.getInt("Dist_Code");
        PSCode=bundle.getInt("PS_Code");

                Boolean Arr=bundle.getBoolean("Arr");
                if (Arr) {
                    ChkArr="true";
                }
                else
                {
                    ChkArr="false";
                }

                Boolean Con=bundle.getBoolean("Con");
                if (Con) {
                    ChkCon="true";
                }
                else
                {
                    ChkCon="false";
                }

                Boolean PO=bundle.getBoolean("PO");
                if (PO) {
                    ChkPO="true";
                }
                else
                {
                    ChkPO="false";
                }

                Boolean CS=bundle.getBoolean("CS");
                if (CS) {
                    ChkCS="true";
                }
                else
                {
                    ChkCS="false";
                }
                Boolean HO=bundle.getBoolean("HO");
                if (HO) {
                    ChkHO="true";
                }
                else
                {
                    ChkHO="false";
                }

        try {
            GetAuthDetailWebService();

        } catch (Exception e) {
            e.printStackTrace();
        }

        btn_Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        listview_person_search.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //SharedPreferences sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                Intent intent=new Intent(PersonDisplay.this,PersonDetails.class);


                intent.putExtra("FULL_NAME",personSearchAdapter.getItem(position).getFULL_NAME());
                intent.putExtra("GENDER",personSearchAdapter.getItem(position).getGENDER());
                intent.putExtra("ALIAS",personSearchAdapter.getItem(position).getALIAS1());
                intent.putExtra("RELIGION",personSearchAdapter.getItem(position).getRELIGION());
                intent.putExtra("RELATION_TYPE",personSearchAdapter.getItem(position).getRELATION_TYPE());
                intent.putExtra("RELATIVE_NAME",personSearchAdapter.getItem(position).getRELATIVE_NAME());
                intent.putExtra("AGE",personSearchAdapter.getItem(position).getAGE());
                intent.putExtra("HEIGHT_FROM_CM",personSearchAdapter.getItem(position).getHEIGHT_FROM_CM());
                intent.putExtra("HEIGHT_TO_CM",personSearchAdapter.getItem(position).getHEIGHT_TO_CM());
                intent.putExtra("STATE_ENG",personSearchAdapter.getItem(position).getSTATE_ENG());
                intent.putExtra("DISTRICT",personSearchAdapter.getItem(position).getDISTRICT());
                intent.putExtra("PS",personSearchAdapter.getItem(position).getPS());
                intent.putExtra("NATIONALITY",personSearchAdapter.getItem(position).getNATIONALITY());
                intent.putExtra("MatchingCriteria",personSearchAdapter.getItem(position).getMatchingCriteria());
                intent.putExtra("REGISTRATION_NUM",personSearchAdapter.getItem(position).getREGISTRATION_NUM());
                intent.putExtra("REG_DT",personSearchAdapter.getItem(position).getREG_DT());
                intent.putExtra("REG_NUM",personSearchAdapter.getItem(position).getREG_NUM());
                intent.putExtra("UID_NUM",personSearchAdapter.getItem(position).getUID_NUM());
                intent.putExtra("PERMANENT",personSearchAdapter.getItem(position).getPERMANENT());
                intent.putExtra("PRESENT",personSearchAdapter.getItem(position).getPRESENT());
                intent.putExtra("UploadedFile",personSearchAdapter.getItem(position).getUploadedFile());
                intent.putExtra("accusedsrNo",personSearchAdapter.getItem(position).getAccusedsrNo());

                startActivity(intent);
            }
        });
            }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        return;
    }


    public void GetAuthDetailWebService() throws Exception {
        this.mProgressDialog.show();
        Map postParams = new HashMap();

        postParams.put("MATCH_CRITERIA_ARR",ChkArr.toString());
        postParams.put("MATCH_CRITERIA_CON",ChkCon.toString());
        postParams.put("MATCH_CRITERIA_PO",ChkPO.toString());
        postParams.put("MATCH_CRITERIA_CS",ChkCS.toString());
        postParams.put("MATCH_CRITERIA_HO",ChkHO.toString());
        postParams.put("P_FName",P_FName.toString());
        postParams.put("P_MName",P_MName.toString());
        postParams.put("P_LName",P_LName.toString());
        postParams.put("P_Alias",P_Alias.toString());
        postParams.put("P_GCODE",P_GCODE);
        postParams.put("P_RELGNCODE",P_RELGNCODE);
        postParams.put("P_RELTYPECODE",P_RELTYPECODE);
        postParams.put("P_RelativeName",P_RelativeName.toString());
        postParams.put("P_AgeFrom",P_AgeFrom);
        postParams.put("P_AgeTo",P_AgeTo);
        postParams.put("P_HEIGHTFROM",P_HEIGHTFROM);
        postParams.put("P_HEIGHTTO",P_HEIGHTTO);
        postParams.put("STATE_CD",STATE_CD);
        postParams.put("DISTRICT_CD",DistCode);
        postParams.put("PS_CD",PSCode);
        postParams.put("PS_CD",PSCode);
        postParams.put("lang_cd", ""+Constants.LANG_CD);
        postParams.put("m_service", "mPersonSearch");


        JSONPostParams jsonPostParams = new JSONPostParams("mPersonSearch", postParams);


        // -----------------------------------------------------------------

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Constants.API_BASE_URL).setLog(new RestAdapter.Log() {
                    @Override
                    public void log(String msg) {
                        Log.i("Res Complaint -", msg);
                    }
                }).setLogLevel(RestAdapter.LogLevel.FULL).build();

        ApiCaller apiCaller = restAdapter.create(ApiCaller.class);

        int cnt;
        apiCaller.mPersonSearch(jsonPostParams, new Callback<WSPLoginConnect>() {

                    @Override
                    public void failure(RetrofitError arg0) {
                        if (mProgressDialog != null && mProgressDialog.isShowing())
                            mProgressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Can not connect to server.", Toast.LENGTH_LONG).show();


                    }// end failure

                    @Override
                    public void success(WSPLoginConnect result, Response response) {

                        if (mProgressDialog != null && mProgressDialog.isShowing())
                            mProgressDialog.dismiss();

                        try {

                            if (result.STATUS_CODE.toString().equals("200")) {



                                TypedInput body = response.getBody();

                                try {

                                    BufferedReader reader = new BufferedReader(new InputStreamReader(body.in()));
                                    out = new StringBuilder();
                                    String newLine = System.getProperty("line.separator");
                                    String line;

                                    while ((line = reader.readLine()) != null) {
                                        out.append(line);
                                        out.append(newLine);
                                    }
                                    System.out.println("out is is " + out.toString());

                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                                String code;

                                try {
                                    JSONObject reader = new JSONObject(out.toString());
                                    JSONArray states = reader.getJSONArray("personSearchCheck");

                                    for (int i = 0; i < states.length(); i++) {

                                        JSONObject jsonObj2 = states.getJSONObject(i);

                                        //java or ms both have different method to get image
                                        String imageName = "UploadedFile"; // for ms stack
                                        if(Constants.API_PROGRAMMING_STACK==Constants.STACK_JAVA) imageName="accusedsrNo";



                                        Utils.printv("jsonObj2 json "+jsonObj2);

                                        Gson gson = new Gson();
                                        PersonSearchInfo objPersonSearchInfo = gson.fromJson(jsonObj2.toString(), PersonSearchInfo.class);

                                        arrayList_person_search.add(objPersonSearchInfo);



//                                        arrayList_person_search.add(new PersonSearchInfo(
//                                                new String(jsonObj2.getString("FULL_NAME")),
//                                                new String(jsonObj2.getString("GENDER")),
//                                                new String(jsonObj2.getString("ALIAS1")),
//                                                new String(jsonObj2.getString("RELIGION")),
//                                                new String(jsonObj2.getString("RELATION_TYPE")),
//                                                new String(jsonObj2.getString("RELATIVE_NAME")),
//                                                new String(jsonObj2.getString("AGE")),
//                                                new String(jsonObj2.getString("HEIGHT_FROM_CM")),
//                                                new String(jsonObj2.getString("HEIGHT_TO_CM")),
//                                                new String(jsonObj2.getString("STATE_ENG")),
//                                                new String(jsonObj2.getString("DISTRICT")),
//                                                new String(jsonObj2.getString("PS")),
//                                                new String(jsonObj2.getString("NATIONALITY")),
//                                                new String(jsonObj2.getString("MatchingCriteria")),
//                                                new String(jsonObj2.getString("REGISTRATION_NUM")),
//                                                new String(jsonObj2.getString("REG_DT")),
//                                                new String(jsonObj2.getString("REG_NUM")),
//                                                new String(jsonObj2.getString("UID_NUM")),
//                                                new String(jsonObj2.getString("PERMANENT")),
//                                                new String(jsonObj2.getString("PRESENT")),
//                                                new String(jsonObj2.getString(imageName))
//                                        ));


                                    } //end forloop

                                    Btnfooter((int)states.length());
                                    loadList(0);
                                    CheckBtnBackGroud(0);


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }


                                System.out.println("RESULT status " + result.STATUS_CODE);


                            } else {
                                if (mProgressDialog != null && mProgressDialog.isShowing())
                                    mProgressDialog.dismiss();
                                Toast.makeText(getApplicationContext(), "Try Again", Toast.LENGTH_LONG).show();


                            }

                        }catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(), "Try Again, timeout or server failure.", Toast.LENGTH_LONG).show();
                        }

                    }// end success

                }

        );

    }

    private void Btnfooter(int count)
    {

        TOTAL_LIST_ITEMS=count;
        int val = TOTAL_LIST_ITEMS%NUM_ITEMS_PAGE;
        val = val==0?0:1;
        noOfBtns=TOTAL_LIST_ITEMS/NUM_ITEMS_PAGE+val;

        LinearLayout ll = (LinearLayout)findViewById(R.id.btnLay);

        btns = new Button[noOfBtns];

        for(int i=0;i<noOfBtns;i++)
        {
            btns[i] =   new Button(this);
            btns[i].setBackgroundColor(getResources().getColor(android.R.color.transparent));
            btns[i].setText(""+(i+1));

            LinearLayout.LayoutParams lp = new     LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            ll.addView(btns[i], lp);

            final int j = i;
            btns[j].setOnClickListener(new View.OnClickListener() {

                public void onClick(View v)
                {
                    loadList(j);
                    CheckBtnBackGroud(j);
                }
            });
        }

    }


    private void loadList(int number)
    {
        sort.clear();
        //ArrayList<String> sort = new PropertySearchInfo(String, String, String reg_Owner, String MV_Type, String REGISTRATION_NO, String CHASSIS_NO, String ENGINE_NO, String full_FIR_NUMBER, String state, String district, String ps, String propertyNature, String mvModel, String mvMake, String mvColor);
        int start = number * NUM_ITEMS_PAGE;
        for(int i=start;i<(start)+NUM_ITEMS_PAGE;i++)
        {
            if(i<arrayList_person_search.size())
            {
                sort.add(arrayList_person_search.get(i));
            }
            else
            {
                break;
            }
        }
        personSearchAdapter= new PersonSearchAdapter(getApplicationContext(),sort);
        listview_person_search.setAdapter(personSearchAdapter);
    }


    private void CheckBtnBackGroud(int index)
    {
        title.setText("Page "+(index+1)+" of "+noOfBtns);
        for(int i=0;i<noOfBtns;i++)
        {
            if(i==index)
            {
                // btns[index].setBackgroundDrawable(getResources().getDrawable(R.drawable.box_green));
                //btns[index].setTextColor(getResources().getColor(android.R.color.holo_orange_light));
                btns[i].setTextColor(getResources().getColor(android.R.color.white));
            }
            else
            {
                btns[i].setBackgroundColor(getResources().getColor(android.R.color.transparent));
                btns[i].setTextColor(getResources().getColor(android.R.color.black));
            }
        }

    }
}
