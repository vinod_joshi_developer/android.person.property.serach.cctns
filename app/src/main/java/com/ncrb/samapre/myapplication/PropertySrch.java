package com.ncrb.samapre.myapplication;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedInput;

public class PropertySrch extends AppCompatActivity {
Spinner sp1;
String search="vehicle";
    ArrayList<String> data = new ArrayList<>();
    StringBuilder out;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_property);
        sp1=(Spinner) findViewById(R.id.sp_Type_Vehicle);
        try {
            GetAuthDetailWebService(search);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }




    public void GetAuthDetailWebService(String username1) throws Exception {

        Map postParams = new HashMap();
        postParams.put("hello", username1.toString());
        postParams.put("m_service", "mVehicleTypeConnect");


        // -----------------------------------------------------------------

        // create a new hash which you want to send on server
        Map postParamsSeed = new HashMap();

        //
        //postParamsSeed.put("seed", "Y7xdxluF9CcdxY2Puao4Uy7wA37J9+REL68AGUqYPt9G1JW94dVnq5sb5FNMwnDVg/+MLik/94Tl2HgdM1+e7g==");
        //postParams.put("seed", "q8vUuHBQzW3pIIXG7WOE0bKDBEEAxPf0n9tHQw2aISJS1FS17AQzUDNc8FTTaOQCi8UX7VpJfZgteIgT9qaPXA==");

        System.out.println("hello user " + username1);
        JSONPostParams jsonPostParams = new JSONPostParams("mVehicleTypeConnect", postParams);


        // -----------------------------------------------------------------

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Constants.API_BASE_URL).setLog(new RestAdapter.Log() {
                    @Override
                    public void log(String msg) {
                        Log.i("Res Complaint -", msg);
                    }
                }).setLogLevel(RestAdapter.LogLevel.FULL).build();

        ApiCaller apiCaller = restAdapter.create(ApiCaller.class);

        // -----------------------------------------------------------------

        apiCaller.mVehicleTypeConnect(jsonPostParams, new Callback<WSPLoginConnect>() {

            @Override
            public void failure(RetrofitError arg0) {

                Toast.makeText(getApplicationContext(), "Can not connect to server.", Toast.LENGTH_LONG);


            }// end failure

            @Override
            public void success(WSPLoginConnect result, Response response) {
                TypedInput body = response.getBody();
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(body.in()));
                    out = new StringBuilder();
                    String newLine = System.getProperty("line.separator");
                    String line;

                    while ((line = reader.readLine()) != null) {
                        out.append(line);
                        out.append(newLine);
                    }
                    System.out.println("out is is " + out.toString());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                String code;
                try {
                    JSONObject reader = new JSONObject(out.toString());
                    JSONArray states   = reader.getJSONArray("vehicleTypeCheck");

                    for (int i = 0; i < states.length(); i++) {
                        JSONObject jsonObj2 = states.getJSONObject(i);
                        data.add(new String(jsonObj2.getString("VehicleType")));


                    }
                    System.out.println("CODE is is " + data);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item,
                                data); //selected item will look like a spinner set from XML
                spinnerArrayAdapter.setDropDownViewResource(android.R.layout
                        .simple_spinner_dropdown_item);
                sp1.setAdapter(spinnerArrayAdapter);


                System.out.println("RESULT status " + result.STATUS_CODE);

                if (result.STATUS_CODE.toString().equals("200")) {

                    //System.out.println("RESULT VJ getting offices");

                    //Intent intent = new Intent(PropertySrch.this, Home.class);
                   // startActivity(intent);

                    //Toast.makeText(getApplicationContext(), "Bingo!!! ", Toast.LENGTH_SHORT);


                } else {
                    Toast.makeText(getApplicationContext(),"Try Again", Toast.LENGTH_LONG);
                    //Toast.makeText(getApplicationContext(), result.STATUS, Toast.LENGTH_SHORT);
                }
            }// end success
        });

    }
}